package com.taggstar.rewards;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EligibleServiceMain {


	private static final String TECHNICAL_FAILURE = "Technical Failure Exception";
	private static final String INVALID_ACCOUNT_NUMBER = "Invalid Account Number Exception";

	public static void main(String[] args) {
		int customerNumber = 10;
		Customer customer = new Customer(customerNumber);
		Channel channel1 = new Channel("SPORTS",true);
		Channel channel2 = new Channel("MUSIC",true);
		Channel channel3 = new Channel("KIDS",false);
		Channel channel4 = new Channel("NEWS",true);
		Channel channel5 = new Channel("MOVIES",false);
		
		List<Channel> channelList = new ArrayList<Channel>();
		channelList.add(channel1);
		channelList.add(channel2);
		channelList.add(channel3);
		channelList.add(channel4);
		channelList.add(channel5);
		Portfolio portfolio = new Portfolio(customer,channelList);
		
		//Customer Portfolio is the input to get rewards. This input should come from client
		getRewards(portfolio);
	}

	private static void getRewards(Portfolio portfolio) {
		StringBuilder sb = new StringBuilder();
		try
		{
			Customer customer = portfolio.getCustomer();
			List<Integer> customerList = customer.getCustomerList();
			
			int accountNumber = customer.getAccountNumber();
			sb.append("Customer Number : "+accountNumber+"\n");
			if(customerList.contains(accountNumber))
			{
				RewardService rewardService = new RewardService(portfolio);
				IEligibilityService eligibleServices = rewardService.getEligibleRewards();
				
				Map<String, String> eligibleRewards = eligibleServices.getEligibleRewards();
				for (String key : eligibleRewards.keySet()) {
					String reward = eligibleRewards.get(key);
					
					if(reward == null || reward.isEmpty())
					{
						throw new TechnicalFailureException(TECHNICAL_FAILURE);
					}
					
					else if("N/A".equalsIgnoreCase(reward))
					{
						sb.append("Channel Subscription : "+key+"\n");
						sb.append("EligibilityService Output :  CUSTOMER_INELIGIBLE\n");
						sb.append("Description :  Customer is not eligible\n");
						sb.append("Rewards Service Result :  Return No Rewards\n");
					}
					else
					{
						sb.append("Channel Subscription : "+key+"\n");
						sb.append("EligibilityService Output :  CUSTOMER_ELIGIBLE\n");
						sb.append("Description :  Customer is eligible\n");
						sb.append("Rewards Service Result :"+reward+"\n");
						
					}
					sb.append("\n");
				}
				
			}
			else
			{
				throw new InvalidAccountNumberException(INVALID_ACCOUNT_NUMBER);
			}
		}
		catch(InvalidAccountNumberException | TechnicalFailureException e)
		{
			switch(e.getMessage())
			{
				case INVALID_ACCOUNT_NUMBER: 
					sb.append("EligibilityService Output :  Invalid Account Number exception\n");
					sb.append("Description :  The supplied account number is invalid\n");
					sb.append("Rewards Service Result : No Reward, Account number is invalid\n");
					break;
				case TECHNICAL_FAILURE:
					sb.append("EligibilityService Output :  Technical Failure Exception\n");
					sb.append("Description :  Service Technical Failure\n");
					sb.append("Rewards Service Result :  Return No Rewards\n");
					break;
			}
			e.printStackTrace();
		}
		System.out.println(sb.toString());
	}
}
