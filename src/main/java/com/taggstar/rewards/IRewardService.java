package com.taggstar.rewards;

public interface IRewardService {
	
	public IEligibilityService getEligibleRewards() throws InvalidAccountNumberException,TechnicalFailureException;

}
