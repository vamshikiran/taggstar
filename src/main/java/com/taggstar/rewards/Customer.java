package com.taggstar.rewards;

import java.util.ArrayList;
import java.util.List;

public class Customer {
	
	private int accountNumber;
	private List<Integer> customerList;
	

	public List<Integer> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<Integer> customerList) {
		if(customerList == null)
		{
			customerList = new ArrayList<Integer>();
		}
		customerList.add(1);
		customerList.add(2);
		customerList.add(3);
		this.customerList = customerList;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Customer(int accountNumber) {
		super();
		setCustomerList(null);
		this.accountNumber = accountNumber;
	}
	
	

}
