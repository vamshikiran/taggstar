package com.taggstar.rewards;

import java.util.HashMap;
import java.util.Map;

public class Reward {
	
	private Map<String,String> rewardMap;
	
	public Reward()
	{
		setRewardMap(rewardMap);
	}

	public Map<String, String> getRewardMap() {
		return rewardMap;
	}

	public void setRewardMap(Map<String, String> rewardMap) {
		if(rewardMap == null)
		{
			rewardMap = new HashMap<String, String>();
			rewardMap.put("SPORTS", "CHAMPIONS_LEAGUE_FINAL_TICKET");
			rewardMap.put("KIDS", "N/A");
			rewardMap.put("MUSIC", "KARAOKE_PRO_MICROPHONE");
			rewardMap.put("NEWS", "N/A");
			rewardMap.put("MOVIES", "PIRATES_OF_THE_CARABIAN_COLLECTION");
		}
		this.rewardMap = rewardMap;
	}
	
	
	

}
