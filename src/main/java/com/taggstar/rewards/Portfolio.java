package com.taggstar.rewards;

import java.util.List;

public class Portfolio {
	
	private Customer customer;
	private List<Channel> subscribedChannel;
	
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public List<Channel> getSubscribedChannel() {
		return subscribedChannel;
	}
	public void setSubscribedChannel(List<Channel> subscribedChannel) {
		this.subscribedChannel = subscribedChannel;
	}
	public Portfolio(Customer customer, List<Channel> subscribedChannel) {
		super();
		this.customer = customer;
		this.subscribedChannel = subscribedChannel;
	}
	
	
	

}
