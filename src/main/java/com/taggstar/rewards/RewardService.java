package com.taggstar.rewards;


public class RewardService implements IRewardService {
	
	private Portfolio portfolio;

	public RewardService(Portfolio portfolio) {
		super();
		this.portfolio = portfolio;
	}

	public IEligibilityService getEligibleRewards() throws InvalidAccountNumberException, TechnicalFailureException {
		
		IEligibilityService eligibilityService = new EligibilityService();
		return eligibilityService.getEligibleRewards(portfolio);
	}
	
	

}
