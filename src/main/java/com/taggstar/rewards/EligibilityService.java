package com.taggstar.rewards;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EligibilityService implements IEligibilityService {
	
	private Map<String,String> eligibleRewards;
	
	
	

	public Map<String, String> getEligibleRewards() {
		return eligibleRewards;
	}



	public void setEligibleRewards(Map<String, String> eligibleRewards) {
		this.eligibleRewards = eligibleRewards;
	}



	public IEligibilityService getEligibleRewards(Portfolio portfolio) throws InvalidAccountNumberException,TechnicalFailureException{
		Map<String,String> eligibleRewards = new HashMap<String, String>();
		Reward reward = new Reward();
		IEligibilityService eligibilityService = new EligibilityService();
		
		List<Channel> subscribedChannel = portfolio.getSubscribedChannel();
		
		Map<String,String> getAllRewards = reward.getRewardMap();
		if(getAllRewards != null)
		{
			for(Channel channel : subscribedChannel)
			{
				if(channel != null && channel.isSubscription())
				{
					String channelName = channel.getChannelName();
					String eligibleReward = getAllRewards.get(channelName);
					if(eligibleReward != null)
					{
						eligibleRewards.put(channelName, eligibleReward);
					}
					else
					{
						eligibleRewards.put(channelName, "N/A");
					}
				}
			}
		}
		eligibilityService.setEligibleRewards(eligibleRewards);
		return eligibilityService;
	}
}
