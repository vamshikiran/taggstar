package com.taggstar.rewards;

public class Channel {
	private String channelName;
	private boolean subscription;
	
	

	public Channel(String channelName, boolean subscription) {
		super();
		this.channelName = channelName;
		this.subscription = subscription;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	
	
	public boolean isSubscription() {
		return subscription;
	}

	public void setSubscription(boolean subscription) {
		this.subscription = subscription;
	}

	@Override
	public String toString() {
		return channelName.toUpperCase();
	}
	
	

}
