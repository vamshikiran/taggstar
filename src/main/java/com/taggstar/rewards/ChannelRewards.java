package com.taggstar.rewards;

public class ChannelRewards {
	private String channel;
	private String rewards;
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getRewards() {
		return rewards;
	}
	public void setRewards(String rewards) {
		this.rewards = rewards;
	}
	
	public ChannelRewards(String channel, String rewards) {
		super();
		this.channel = channel;
		this.rewards = rewards;
	}

}
