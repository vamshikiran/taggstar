package com.taggstar.rewards;

import java.util.Map;

public interface IEligibilityService {
	public IEligibilityService getEligibleRewards(Portfolio portfolio) throws InvalidAccountNumberException, TechnicalFailureException;
	public void setEligibleRewards(Map<String, String> eligibleRewards);
	public Map<String, String> getEligibleRewards();

}
